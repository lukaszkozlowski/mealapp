import 'package:flutter/material.dart';
import '../models/meal.dart';
import '../widgets/meal_item.dart';

class FavouritesScreen extends StatelessWidget {
  final List<Meal> favouritesMeal;

  FavouritesScreen(this.favouritesMeal);

  @override
  Widget build(BuildContext context) {
    if (favouritesMeal.isEmpty) {
      return Center(
        child: Text('You have not favourites yet'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favouritesMeal[index].id,
            title: favouritesMeal[index].title,
            imageUrl: favouritesMeal[index].imageUrl,
            duration: favouritesMeal[index].duration,
            affordability: favouritesMeal[index].affordability,
            complexity: favouritesMeal[index].complexity,
          );
        },
        itemCount: favouritesMeal.length,
      );
    }
  }
}
